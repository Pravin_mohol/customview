package com.app.feng.fixtablelayout.inter;

import android.os.Handler;
import android.os.Message;

/**
 * Created by Pravin Mohol.
 */

public interface ILoadMoreListener {

    void loadMoreData(Message message);

}
