package com.app.feng.fixtablelayout.inter;

import android.widget.TextView;

import java.util.List;

/**
 * Created by Pravin Mohol.
 */

public interface IDataAdapter {

    String getTitleAt(int pos);

    /**
     *
     There are several columns
     * @return
     */
    int getTitleCount();

    /**
     * a few lines
     * @return
     */
    int getItemCount();

    void convertData(int position,List<TextView> bindViews);

    void convertLeftData(int position,TextView bindView);

}
