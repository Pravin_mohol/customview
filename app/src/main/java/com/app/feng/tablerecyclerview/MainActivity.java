package com.app.feng.tablerecyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.feng.fixtablelayout.InviteeFixTableLayout;
import com.app.feng.tablerecyclerview.bean.DataBean;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public String[] title = {"Invitees","9AM","10AM","11AM","12PM","1PM","2PM",
                             "3PM","4M","5PM","6PM","7PM","8PM","9PM","10PM","11PM",
                            "12PM"};

    public List<DataBean> data = new ArrayList<>();


    int currentPage = 1;
    int totalPage = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i = 0; i < 15; i++) {
            data.add(new DataBean("Jeff Hiser"+""+i,"busy","busy",
                    "busy","Free","Free","data6",
                    "data7","data8"));
        }

        final InviteeFixTableLayout fixTableLayout =  findViewById(R.id.fixTableLayout);

        //Make sure to set the Adapter or you can't see the TableLayout
        final FixTableAdapter fixTableAdapter = new FixTableAdapter(title,data);
        fixTableLayout.setAdapter(fixTableAdapter);

        // LoadMoreData if you want to set up please after the setAdapter
      //  fixTableLayout.enableLoadMoreData();

       /* fixTableLayout.setLoadMoreListener(new ILoadMoreListener() {
            @Override
            public void loadMoreData(final Message message) {
                Log.i("feng"," 加载更多 Data --- ");
            // Please open the thread to load the data and send it to fixTableLayout via message.                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (currentPage <= totalPage) {
                            for (int i = 0; i < 5; i++) {
                                data.add(new DataBean("update_id","update_data","data2","data3","data4","data5",
                                                      "data6","data7","data8"));
                            }
                            currentPage++;
                            message.arg1 = 50; // 更新了50条数据
                        } else {
                            message.arg1 = 0;
                        }
                        message.sendToTarget();
                    }
                }).start();
            }
        });*/
    }
}
